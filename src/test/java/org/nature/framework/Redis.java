package org.nature.framework;

import java.net.URI;
import java.net.URISyntaxException;

import redis.clients.jedis.Jedis;

public class Redis {
	public static void main(String[] args) throws URISyntaxException {
		Jedis jedis = new Jedis(new URI("http://192.168.2.129:6379"));
		jedis.auth("root");
		String string = jedis.get("name");
		System.out.println(string);
		
	}
}
