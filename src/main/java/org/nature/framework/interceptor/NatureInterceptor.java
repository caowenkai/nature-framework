package org.nature.framework.interceptor;

public interface NatureInterceptor{
	void intercept(Invocation invocation);
}
