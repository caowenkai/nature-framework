package org.nature.framework.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.nature.framework.cache.InvocationContext;
import org.nature.framework.cache.InvocationTokenStore;
import org.nature.framework.cache.NatureContext;
import org.nature.framework.cache.NatureSession;
import org.nature.framework.util.CastUtil;
import org.nature.framework.util.StringUtil;

public class TokenInterceptor implements NatureInterceptor {

	public void intercept(Invocation invocation) {
		HttpServletRequest request = invocation.getRequest();
		NatureSession natureSession = NatureContext.getNatureCtrl().getSession();
		synchronized (natureSession.getLock()) {
			String token = request.getParameter("token");
			if (StringUtil.isBank(token)) {
				invocation.invoke();
			} else {
				String myToken = CastUtil.castString(natureSession.get("token"));
				if (myToken.equals(token)) {
					natureSession.remove("token");
					invocation.invoke(); 
					InvocationTokenStore.put(token, new InvocationContext(invocation.getReturnValue(),invocation.getTargetObject()));
				} else {
					InvocationContext myInvocation = InvocationTokenStore.getInvocation(token);
					invocation.setReturnValue(myInvocation.getReturnValue());
					invocation.setTargetObject(myInvocation.getTargetObject());
				}
			}
		}
	}
	

}
