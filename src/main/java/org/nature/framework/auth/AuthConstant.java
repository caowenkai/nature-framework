package org.nature.framework.auth;

public class AuthConstant {

	public static final String CURRENT_USER 	= "current_user";
	public static final String CURRENT_PREMS 	= "current_prems";
	public static final String USERNAME			= "USERNAME";
	public static final String PASSWORD			= "PASSWORD";
	public static final String PREMS 			= "PREMS";
	
}
