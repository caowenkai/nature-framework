package org.nature.framework.auth;

import java.util.Set;

import org.nature.framework.core.NatureMap;
import org.nature.framework.helper.ConfigHelper;
import org.nature.framework.helper.DatabaseHelper;
import org.nature.framework.util.CastUtil;
import org.nature.framework.util.StringUtil;

public class AuthDao {
	public static Set<String> getPrems(Object username) {
		String queryPremsSql = ConfigHelper.getQueryPremsSql();
		Set<String> prems;
		if (StringUtil.isNotEmpty(ConfigHelper.getAuthCacheName())) {
			prems=DatabaseHelper.querySet(queryPremsSql,ConfigHelper.getAuthCacheName(), username);
		}else{
			prems=DatabaseHelper.querySet(queryPremsSql, username);
		}
		return prems;
	}

	public static NatureMap getUserInfo(Object username) {
		String queryUserSql = ConfigHelper.getQueryUserInfoSql();
		NatureMap userInfo;
		if (StringUtil.isNotEmpty(ConfigHelper.getAuthCacheName())) {
			userInfo= DatabaseHelper.unique(queryUserSql,ConfigHelper.getAuthCacheName(),username);
		}else{
			userInfo= DatabaseHelper.unique(queryUserSql,username);
		}
		return userInfo;
	}

	public static String getStorePassword(Object username) {
		String queryPasswordSql = ConfigHelper.getQueryPaswordSql();
		String storePassword ;
		if (StringUtil.isNotEmpty(ConfigHelper.getAuthCacheName())) {
		storePassword = CastUtil.castString(DatabaseHelper.uniqueValue(queryPasswordSql,ConfigHelper.getAuthCacheName(), username));
		}else{
			storePassword = CastUtil.castString(DatabaseHelper.uniqueValue(queryPasswordSql, username));
		}
		return storePassword;
	}
	public static Set<String> getAllPrems() {
		String querySystemPrems = ConfigHelper.getQueryAllPremsSql();
		Set<String> allPrems;
		if (StringUtil.isNotEmpty(ConfigHelper.getAuthCacheName())) {
			allPrems = DatabaseHelper.querySet(querySystemPrems,ConfigHelper.getAuthCacheName());
		}else{
			allPrems = DatabaseHelper.querySet(querySystemPrems);
		}
		return allPrems;
	}
}
