package org.nature.framework.db;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.nature.framework.helper.ConfigHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.druid.pool.DruidDataSource;

public class DruidDataSourceManager{
	private static Logger LOGGER = LoggerFactory.getLogger(DruidDataSourceManager.class);
	private static DruidDataSource dataSource=new DruidDataSource();
	
	public static DataSource getDataSource() {
		try {
			dataSource.setDriverClassName(ConfigHelper.getJdbcDriver());
			dataSource.setUrl(ConfigHelper.getJdbcUrl());
			dataSource.setUsername(ConfigHelper.getJdbcUserName());
			dataSource.setPassword(ConfigHelper.getJdbcPassWord());
			dataSource.setFilters(ConfigHelper.getJdbcFilters());
			dataSource.setInitialSize(ConfigHelper.getJdbcInitialSize());
			dataSource.setMinIdle(ConfigHelper.getJdbcMinidle());
			dataSource.setMaxActive(ConfigHelper.getJdbcMaxAction());
			dataSource.setMaxWait(ConfigHelper.getJdbcMaxWait());
			dataSource.setTimeBetweenEvictionRunsMillis(ConfigHelper.getJdbcTimeBetweenEvictionRunsMillis());
			dataSource.setPoolPreparedStatements(ConfigHelper.getJdbcPoolPreparedStatements());
			dataSource.setMaxPoolPreparedStatementPerConnectionSize(ConfigHelper.getJdbcMaxPoolPreparedStatementPerConnectionSize());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSource;
	}

	
}
