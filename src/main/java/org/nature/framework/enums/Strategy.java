package org.nature.framework.enums;

public enum Strategy {
    UUID,
    ASSIGN,
    ADDSELF
}
