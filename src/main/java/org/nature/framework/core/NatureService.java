package org.nature.framework.core;

import java.util.List;

import org.nature.framework.bean.Page;
import org.nature.framework.helper.DatabaseHelper;

public abstract class NatureService{

	public int insert(NatureMap natureMap) {
		return DatabaseHelper.insert(natureMap);
	}
	
	public int insert(NatureMap natureMap,String cacheName) {
		return DatabaseHelper.insert(natureMap,cacheName);
	}

	public int update(NatureMap natureMap) {
		return DatabaseHelper.update(natureMap);
	}
	
	public int update(NatureMap natureMap,String cacheName) {
		return DatabaseHelper.update(natureMap,cacheName);
	}
 
	public int delete(NatureMap natureMap) {
		return DatabaseHelper.delete(natureMap);
	}
	public int delete(NatureMap natureMap,String cacheName) {
		return DatabaseHelper.delete(natureMap,cacheName);
	}

	public <T> T byId(T t) {
		return DatabaseHelper.byId(t);
	}
	public <T> T byId(T t,String cacheName) {
		return DatabaseHelper.byId(t,cacheName);
	}
	
	public NatureMap unique(String sql) {
		return DatabaseHelper.unique(sql);
	}
	public NatureMap unique(String sql,String cacheName) {
		return DatabaseHelper.unique(sql,cacheName);
	}

	public List<NatureMap> list(String sql) {
		return DatabaseHelper.query(sql);
	}
	
	public List<NatureMap> list(String sql,String cacheName) {
		return DatabaseHelper.query(sql,cacheName);
	}
	
	public List<NatureMap> list(String sql,Object...params) {
		return DatabaseHelper.query(sql,params);
	}
	
	public List<NatureMap> list(String sql,String cacheName,Object...params) {
		return DatabaseHelper.query(sql,cacheName,params);
	}

	public Page page(Page page, String sql) {
		return DatabaseHelper.query(page, sql);
	}
	
	public void page(Page page, String sql,String cacheName) {
		DatabaseHelper.query(page, sql,cacheName);
	}

}
