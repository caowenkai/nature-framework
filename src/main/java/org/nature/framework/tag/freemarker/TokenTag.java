package org.nature.framework.tag.freemarker;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import org.nature.framework.cache.NatureContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateModel;

public class TokenTag implements TemplateDirectiveModel{
	private static Logger LOGGER = LoggerFactory.getLogger(TokenTag.class);
	@SuppressWarnings("rawtypes")
	public void execute(Environment en, Map map, TemplateModel[] templateModels, TemplateDirectiveBody templateDirectiveBody){
		String value = UUID.randomUUID().toString().replaceAll("-", "");
		NatureContext.getNatureCtrl().getSession().set("token", value);
		try {
			en.getOut().write("<input type=\"hidden\" name=\"token\" value=\""+value+"\"/>");
		} catch (IOException e) {
			LOGGER.error("Environment's IO is error");
			throw new RuntimeException(e);
		}
	}
}
