package org.nature.framework.helper;

import java.util.Properties;

import org.nature.framework.config.ConfigConstant;
import org.nature.framework.util.PropsUtil;

/**
 * Created by Ocean on 2016/3/9.
 */
public final class ConfigHelper {

    private static final Properties CONFIG_PROPS = PropsUtil.loadProps(ConfigConstant.CONFIG_FILE);

    public static String getJdbcDriver() {
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_DRIVER);
    }

    public static String getJdbcUrl() {
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_URL);
    }

    public static String getJdbcUserName() {
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_USERNAME);
    }

    public static String getJdbcPassWord() {
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_PASSWORD);
    }
    
    public static String getJdbcDataSource() {
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_DATASOURCE);
    }
    
    public static String getJdbcFilters() {
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_FILTERS);
    }
    public static int getJdbcInitialSize() {
        return PropsUtil.getInt(CONFIG_PROPS, ConfigConstant.JDBC_INITIAL_SIZE,5);
    }
    public static int getJdbcMinidle() {
        return PropsUtil.getInt(CONFIG_PROPS, ConfigConstant.JDBC_MINIDLE,5);
    }
    public static int getJdbcMaxAction() {
        return PropsUtil.getInt(CONFIG_PROPS, ConfigConstant.JDBC_MAXACTIVE,20);
    }
    public static long getJdbcMaxWait() {
        return PropsUtil.getLong(CONFIG_PROPS, ConfigConstant.JDBC_MAXWAIT,60000);
    }
    public static long getJdbcTimeBetweenEvictionRunsMillis() {
        return PropsUtil.getLong(CONFIG_PROPS, ConfigConstant.JDBC_TIME_BETWEEN_EVICTION_RUNS_MILLIS,300000);
    }
    public static int getJdbcMaxPoolPreparedStatementPerConnectionSize() {
        return PropsUtil.getInt(CONFIG_PROPS, ConfigConstant.JDBC_MAXPOOL_PREPARED_STATEMENT_PER_CONNECTION_SIZE,20);
    }
    public static boolean getJdbcPoolPreparedStatements() {
        return PropsUtil.getBoolean(CONFIG_PROPS, ConfigConstant.JDBC_POOL_PREPARED_STATEMENTS,true);
    }
    public static int getJdbcDefaultTransactionIsolation() {
        return PropsUtil.getInt(CONFIG_PROPS, ConfigConstant.JDBC_DEFAULT_TRANSACTION_ISOLATION,2);
    }
    
    

    public static String getAppBasePackage() {
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.NATURE_BASE_PACKAGE);
    }

    public static String getAppPagePath() {
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.NATURE_PAGE_PATH, "/WEB-INF/view/");
    }
    
    public static String getExcludesPattern() {
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.NATURE_EXCLUDES_PATTERN);
    }

    public static String getFileEncoding(){
    	return System.getProperty("file.encoding","UTF-8");
    }
    
    public static String getCacheManager(){
    	return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.DB_CACHE_MANAGER);
    }
    
    public static boolean getDbAutocreate() {
        return PropsUtil.getBoolean(CONFIG_PROPS, ConfigConstant.DB_AUTOCREATE, false);
    }
    
    public static boolean getI18n() {
        return PropsUtil.getBoolean(CONFIG_PROPS, ConfigConstant.NATURE_I18N, false);
    }
    /**
     * 默认值100兆
     */
    public static int getMultipartMaxSize(){
    	return PropsUtil.getInt(CONFIG_PROPS, ConfigConstant.NATURE_MULTIPART_MAXSIZE, 100*1024*1024);
    }
    
    public static boolean getAuth() {
        return PropsUtil.getBoolean(CONFIG_PROPS, ConfigConstant.NATURE_AUTH, false);
    }

	public static String getQueryPaswordSql() {
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.AUTH_QUERY_PASSWORD_SQL);
	}

	public static String getQueryUserInfoSql() {
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.AUTH_QUERY_USERINFO_SQL);
	}

	public static String getQueryPremsSql() {
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.AUTH_QUERY_PREMS_SQL);
	}

	public static String getQueryAllPremsSql() {
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.AUTH_QUERY_ALL_PREMS_SQL);
	}

	public static String getNoLoginUrl() {
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.AUTH_NO_LOGIN_URL);
	}

	public static String getNoPremUrl() {
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.AUTH_NO_PREM_URL);
	}
	
	public static String getAuthCacheName() {
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.AUTH_CACHE_NAME);
	}
	
    
}
