package org.nature.framework.cache;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class InvocationTokenStore {
	private static String TOKEN_MAP = "TOKEN_MAP";
	
	public static void put(String token,InvocationContext invocationContext) {
		Map<String, InvocationContext> map = new HashMap<String, InvocationContext>();
		map.put(token, invocationContext.serialize());
		setInvocationMap(map);
	}


	private static void setInvocationMap(Map<String, InvocationContext> map) {
		NatureSession session = NatureContext.getNatureCtrl().getSession();
		session.set(TOKEN_MAP, map);
	} 

	@SuppressWarnings("unchecked")
	private static Map<String, InvocationContext> getInvocationMap() {
		NatureSession session = NatureContext.getNatureCtrl().getSession();
		Object object = session.get(TOKEN_MAP);
		if (object==null) {
			object = Collections.EMPTY_MAP;
		}
		return (Map<String, InvocationContext>) object;
	}

	public static InvocationContext getInvocation(String token) {
		Map<String, InvocationContext> invocationMap = getInvocationMap();
		return invocationMap.get(token);
	}

	public static void remove(String key) {
		Map<String, InvocationContext> invocationMap = getInvocationMap();
		invocationMap.remove(key);
	}
	
	public static void removeAll() {
		NatureSession session = NatureContext.getNatureCtrl().getSession();
		session.remove(TOKEN_MAP);
	}
	
	
}
