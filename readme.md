 nature-framework是一个适合极速开发的mvc+orm框架，无get/set方法，支持拦截器，ioc，声明式事务，隔离级别，i18n国际化，表单防重复提交令牌，c3p0、druid数据源，支持自动建表，支持数据层ehcache数据缓存，页面支持jsp，freemarker等；后期将会支持redis缓存，dubbo分布式服务，shro权限控制等。
 详细请参考示例：http://git.oschina.net/mvilplss/nature-demo
  **框架当前支持servlet3.0规范，建议使用jdk8.0和tomcat8.0或更高版本。** 